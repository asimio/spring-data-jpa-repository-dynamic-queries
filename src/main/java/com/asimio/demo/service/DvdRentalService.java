package com.asimio.demo.service;

import java.util.List;

import com.asimio.demo.dto.FilmDto;

public interface DvdRentalService {

    List<FilmDto> retrieveFilms(FilmSearchCriteria searchCriteria);
}