package com.asimio.demo.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.asimio.demo.dao.FilmDao;
import com.asimio.demo.dto.FilmDto;
import com.asimio.demo.service.DvdRentalService;
import com.asimio.demo.service.FilmSearchCriteria;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
@Transactional(readOnly = true)
public class DefaultDvdRentalService implements DvdRentalService {

    private final FilmDao filmDao;

    @Override
    public List<FilmDto> retrieveFilms(FilmSearchCriteria searchCriteria) {
        return this.filmDao.findAll(searchCriteria);
    }
}