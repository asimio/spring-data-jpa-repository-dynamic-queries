# README #

Accompanying source code for blog entry at https://tech.asimio.net/2021/07/26/Writing-dynamic-SQL-queries-using-Spring-Data-JPA-Repositories-and-EntityManager.html

### Requirements ###

* Java 8+
* Maven 3.2.x+

### Building the artifact ###

```
mvn clean package
```

### Running the application from command line ###

```
mvn spring-boot:run
```

### Available URLs

```
curl "http://localhost:8080/api/films"
curl "http://localhost:8080/api/films?minRentalRate=0.5&maxRentalRate=4.99"
curl "http://localhost:8080/api/films?releaseYear=2006"
curl "http://localhost:8080/api/films?category=Action&category=Comedy&category=Horror&minRentalRate=0.99&maxRentalRate=4.99&releaseYear=2005"
```
should result in successful responses. Please look at the logs to verify Hibernate executes different queries depending on the request parameters.

### Who do I talk to? ###

* orlando.otero at asimio dot net
* https://www.linkedin.com/in/ootero
